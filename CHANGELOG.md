# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.3.2](https://github.com/juniorpaiva95/posto-livre-admin/compare/v1.3.1...v1.3.2) (2020-07-28)


### Bug Fixes

* **fix-load:** correçao de atualizaçao da lista de unidade apos atualizar ([6f7cf3f](https://github.com/juniorpaiva95/posto-livre-admin/commit/6f7cf3f70030ef2c238941a733e171b75258f278))

### [1.3.1](https://github.com/juniorpaiva95/posto-livre-admin/compare/v1.3.0...v1.3.1) (2020-07-28)


### Features

* **filter-range:** filtro de periodo na lista de lotes ([942ba2a](https://github.com/juniorpaiva95/posto-livre-admin/commit/942ba2ab57caa0d5c8a26f63f1690ed2c0a59c25))
* **update-unit:** funcionalidade para atualizar unidade ([319d33d](https://github.com/juniorpaiva95/posto-livre-admin/commit/319d33d42105b4a75aadb109270a85592d713718))


### Bug Fixes

* **fix import:** ajuste de import no app.module ([ed45cbc](https://github.com/juniorpaiva95/posto-livre-admin/commit/ed45cbcc455782e2462dc6b6567ef826949a1e8c))

## [1.3.0](https://github.com/juniorpaiva95/posto-livre-admin/compare/v1.2.0...v1.3.0) (2020-07-27)


### Features

* **auction-module:** melhoria no modulo de usuario, implementaçao de auction module ([5c2e3bc](https://github.com/juniorpaiva95/posto-livre-admin/commit/5c2e3bccee50588091ae6fc6501da12f4c5fd284))

## [1.2.0](https://github.com/juniorpaiva95/posto-livre-admin/compare/v1.1.0...v1.2.0) (2020-07-27)


### Features

* **lista,usuario,unidade:** melhoria nas listagens de usuario, combustivel e unidade ([e2a9412](https://github.com/juniorpaiva95/posto-livre-admin/commit/e2a94125e768c1274a527f934fc6ed74da9fd399))

## [1.1.0](https://github.com/juniorpaiva95/posto-livre-admin/compare/v1.0.0...v1.1.0) (2020-07-26)


### Features

* **modulo faq:** implementaçao do modulo de Faq ([7bba954](https://github.com/juniorpaiva95/posto-livre-admin/commit/7bba954fa9ebd58daa68e251b314832f64144b02))

## 1.0.0 (2020-07-26)
