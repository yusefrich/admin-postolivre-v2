import { Component, OnInit } from '@angular/core';
import * as Pusher from 'pusher-js';
import { PusherService } from '../shared/services/pusher.service';
import { AuthenticationService } from '../shared/services/authentication.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
})

export class DashboardComponent implements OnInit {

    public isConnected: boolean = false;
    public pusher: any;
    constructor(private pusherService: PusherService, private authService: AuthenticationService) {

    }

    ngOnInit(): void {
        this.pusherService.isConnectedChanges.subscribe(isConnected => this.isConnected = isConnected);
    }

    disconnect() {
        this.pusherService.disconnect();
    }

    

}
