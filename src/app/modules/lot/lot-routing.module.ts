import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LotListComponent } from './lot-list/lot-list.component';



const routes: Routes = [
    {
        path: '',
        component: LotListComponent,
        data: {
            title: 'Lotes'
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LotRoutingModule { }
