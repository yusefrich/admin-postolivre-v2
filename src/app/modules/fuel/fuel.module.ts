import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FuelListComponent } from './fuel-list/fuel-list.component';
import { FuelRoutingModule } from './fuel-routing.module';
import { NzLayoutModule, NzCardModule, NzTableModule, NzBadgeModule, NzAvatarModule, NzSelectModule, NzInputModule, NzButtonModule, NzIconModule, NzPopconfirmModule, NzToolTipModule, NzModalModule, NzSpinModule, NzFormModule, NzTagModule } from 'ng-zorro-antd';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TableService } from 'src/app/shared/services/table.service';
import { FuelUpdateComponent } from './fuel-update/fuel-update.component';

const antdModules = [
  NzLayoutModule,
  NzCardModule,
  NzTableModule,
  NzBadgeModule,
  NzAvatarModule,
  NzSelectModule,
  NzInputModule,
  NzButtonModule,
  NzIconModule,
  NzPopconfirmModule,
  NzToolTipModule,
  NzModalModule,
  NzSpinModule,
  NzFormModule,
  NzTagModule
];

@NgModule({
  declarations: [FuelListComponent, FuelUpdateComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    FuelRoutingModule,
    ...antdModules
  ],
  providers: [TableService]
})
export class FuelModule { }
