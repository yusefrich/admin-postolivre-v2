import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FuelService } from 'src/app/shared/services/api/fuel.service';
import { delay, finalize } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';

interface Fuel {
  name: string;
  color_hex: string;
  slug: string;
  identifier: number;
  id: string;
}

@Component({
  selector: 'app-fuel-update',
  templateUrl: './fuel-update.component.html',
})
export class FuelUpdateComponent implements OnInit {

  @Input() entity: Fuel;
  form: FormGroup;
  isLoading: boolean = false;
  autoTips: Record<string, Record<string, string>> = {
    en: {
      required: 'Campo obrigatório',
      email: 'Email inválido'
    }
  };

  constructor(private fb: FormBuilder, private fuelService: FuelService, private modal: NzModalRef) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      "name": this.fb.control(this.entity.name),
      "slug": this.fb.control(this.entity.slug),
      "color_hex": this.fb.control(this.entity.color_hex, Validators.compose([Validators.pattern("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$")]))
    });
  }

  submitForm(): void {
    //@TODO: Call to recursive validate form
    this.isLoading = true;
    this.fuelService.update({ endpoint: 'fuel', id: this.entity.id }, this.form.value)
    .pipe(delay(1000), finalize(() => this.isLoading = false))
    .subscribe((result) => this.destroyModal(result));
  }

  destroyModal(data): void {
    this.modal.destroy(data);
  }

}
