import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FuelService } from 'src/app/shared/services/api/fuel.service';
import { TableService } from 'src/app/shared/services/table.service';
import { NzNotificationService, NzModalService } from 'ng-zorro-antd';
import { FuelUpdateComponent } from '../fuel-update/fuel-update.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
interface DataItem {
  id: string;
  name: string;
  created_at: string;
  updated_at: string;
  slug: string;
  color_hex: string;
}
@Component({
  selector: 'app-fuel-list',
  templateUrl: './fuel-list.component.html',
})
export class FuelListComponent implements OnInit {

  allChecked: boolean = false;
  indeterminate: boolean = false;
  displayData = [];
  searchInput: string;
  isVisible: boolean = false;
  formCreate: FormGroup;
  autoTips: Record<string, Record<string, string>> = {
    en: {
      required: 'Campo obrigatório',
      email: 'Email inválido'
    }
  };

  orderColumn = [
    {
      title: 'ID',
      compare: (a: DataItem, b: DataItem) => a.id.localeCompare(b.id),
    },
    {
      title: 'Tipo',
      compare: (a: DataItem, b: DataItem) => a.name.localeCompare(b.name)
    },
    {
      title: 'Cor hexadecimal',
      compare: (a: DataItem, b: DataItem) => a.color_hex.localeCompare(b.color_hex)
    },
    {
      title: 'Slug',
      compare: (a: DataItem, b: DataItem) => a.slug.localeCompare(b.slug),
    },
    {
      title: 'Data de criação',
      compare: (a: DataItem, b: DataItem) => a.created_at.localeCompare(b.created_at)
    },
    {
      title: 'Última atualização',
      compare: (a: DataItem, b: DataItem) => a.updated_at.localeCompare(b.updated_at)
    },
    {
      title: ''
    }
  ]

  fuelList = [];

  constructor(
    private tableSvc: TableService, 
    private fuelService: FuelService, 
    private alertService: NzNotificationService,
    private modalService: NzModalService,
    private viewContainerRef: ViewContainerRef,
    private fb: FormBuilder
  ) {
    this.formCreate = this.fb.group({
      "name": this.fb.control("", Validators.required),
      "slug": this.fb.control("", Validators.required),
      "color_hex": this.fb.control("", Validators.compose([Validators.required, Validators.pattern("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$")]))
    });
  }

  search() {
    this.displayData = this.tableSvc.search(this.searchInput, this.fuelList)
  }

  delete(item) {
    this.fuelService.delete({ endpoint: 'fuel', id: item.id }).subscribe(() => {
      this.alertService.success("Sucesso!", `Combustível ${item.name} removido com sucesso!`);
      this.loadData()
    });
  }

  ngOnInit() { 
    this.loadData();
  }

  loadData() {
    this.fuelService.read({ endpoint: 'fuel' }).subscribe((data: any) => {
      this.displayData = data.fuels
      this.fuelList = data.fuels
    });
  }

  showModalUpdate(item) {
    console.log(item);
    const modal = this.modalService.create({
      nzTitle: `Atualizar Combustível - ${item.name}`,
      nzContent: FuelUpdateComponent,
      nzViewContainerRef: this.viewContainerRef,
      nzComponentParams: {
        entity: item
      },
      nzOnOk: () => new Promise(resolve => setTimeout(resolve, 1000)),
      nzFooter: [
        {
          label: 'Salvar',
          type: 'primary',
          loading: componentInstance => componentInstance.isLoading,
          onClick: componentInstance => {
            componentInstance.submitForm()
          }
        }
      ]
    })

    modal.afterClose.subscribe(result => {
      if(result) {
        this.loadData() 
      }      
    });
  }

  // Create Fuel

  toggleModal() {
    this.isVisible = !this.isVisible;
  }

  handleOk(): void {
    for (const i in this.formCreate.controls) {
      this.formCreate.controls[i].markAsDirty();
      this.formCreate.controls[i].updateValueAndValidity();
    }
    if (this.formCreate.valid) {
      this.fuelService.create({ endpoint: 'fuel' }, this.formCreate.value).subscribe(() => {
        this.isVisible = false
        this.formCreate.reset();
        this.alertService.success("Sucesso", "Combustível cadastrado com sucesso!");
        this.loadData()
      })
    }
  }

  handleCancel(): void {
    this.isVisible = false;
  }

}
