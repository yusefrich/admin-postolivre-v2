import { AuctionService } from 'src/app/shared/services/api/auction.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UnitService } from 'src/app/shared/services/api/unit.service';
import { delay, finalize } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';
import { Observable } from 'rxjs';
import { FuelService } from 'src/app/shared/services/api/fuel.service';

@Component({
  selector: 'app-auction-update',
  templateUrl: './auction-update.component.html',
  styleUrls: ['./auction-update.component.css']
})
export class AuctionUpdateComponent implements OnInit {
  
  form: FormGroup;
  paymentForm: FormGroup;
  paymentFile: FormData;
  fuelList: any[];

  @Input() auction: any;
  isLoading: boolean = false;

  autoTips: Record<string, Record<string, string>> = {
    en: {
      required: 'Campo obrigatório'
    }
  };

  constructor(private fb: FormBuilder, 
              private auctionService: AuctionService, 
              private unitService: UnitService, 
              private fuelService: FuelService,
              private modal: NzModalRef) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      "freight_type": this.fb.control(this.auction.freight_type, [Validators.required]),
      "port": this.fb.group({        
        "name": [this.auction.port.name, [Validators.required]],
      }),
      "fuel": this.fb.group({        
        "id": [this.auction.fuel.id, [Validators.required]],
      }),
      "fuel_amount": this.fb.control(this.auction.fuel_amount,  [Validators.required]),
    })

    this.paymentForm = this.fb.group({
      file: ['']
    })
    this.fuelService.read({ endpoint: 'fuel' }).subscribe((data: any) => this.fuelList = data.fuels);

  }
  onFileSelect(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.paymentForm.get('file').setValue(file);
      /* this.paymentFile.append('file', file); */
      /* var formData = new FormData();
      formData.append('file', file);
      this.paymentFile = formData; */

    }
  }

  submitForm(): void {
    this.isLoading = true;
    this.auctionService.update({ endpoint: 'auctions', id: this.auction.id }, this.form.value)
    .pipe(delay(1000), finalize(() => this.isLoading = false))
    .subscribe((result) => this.destroyModal(result));
  }
  submitPaymentVoucher() :void {
    const formData = new FormData();
    formData.append('file', this.paymentForm.get('file').value);

    console.log("submitPaymentVoucher being called ok")


    this.isLoading = true;
    this.auctionService.upload({ endpoint: `auctions/${this.auction.id}/upload` }, formData)
    .pipe(delay(1000), finalize(() => this.isLoading = false))
    .subscribe((result) => this.destroyModal(result));
  }

  destroyModal(data): void {
    this.modal.destroy(data);
  }

}
