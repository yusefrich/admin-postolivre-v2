import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { QuestionService } from 'src/app/shared/services/api/question.service';
import { delay, finalize } from 'rxjs/operators';
import { NzModalRef, NzNotificationService } from 'ng-zorro-antd';

@Component({
  selector: 'app-faq-update',
  templateUrl: './faq-update.component.html',
  styleUrls: ['./faq-update.component.css']
})
export class FaqUpdateComponent implements OnInit {

  @Input() faq: any;
  form: FormGroup;
  isLoading: boolean = false;
  autoTips: Record<string, Record<string, string>> = {
    en: {
      required: 'Campo obrigatório',
      email: 'Email inválido'
    }
  };

  constructor(
    private fb: FormBuilder, 
    private questionService: QuestionService, 
    private modal: NzModalRef
  ) {}

  ngOnInit(): void {
    console.log(this.faq);
    this.form = this.fb.group({
      "question": this.fb.control(this.faq?.question),
      "answers": this.fb.array([])
    })

    this.faq.answers.answers.map((answer) => {
      (this.form.get('answers') as FormArray).push(
        this.fb.group({
          "answer": this.fb.control(answer.answer)
        })
      );
    });
    console.debug(this.form);
  }

  createAnswerGroup(): FormGroup {
    return this.fb.group({
      "answer": this.fb.control(null, Validators.required)
    });
  }

  removeField(index: number, e: MouseEvent): void {
    e.preventDefault();
    
    let respostas = this.form.get('answers') as FormArray;
    if (respostas.length > 1) {
      respostas.removeAt(index);
    }
  }

  addAnswer() {
    (this.form.get('answers') as FormArray).push(this.createAnswerGroup());
  }

  submitForm(): void {
    //@TODO: Call to recursive validate form
    this.isLoading = true;
    this.questionService.update({ endpoint: 'questions', id: this.faq.id }, this.form.value)
    .pipe(delay(1000), finalize(() => this.isLoading = false))
    .subscribe((result) => this.destroyModal(result));
  }

  destroyModal(data): void {
    this.modal.destroy(data);
  }

}
