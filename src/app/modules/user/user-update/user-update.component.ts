import { Component, OnInit, Input, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { UserService } from 'src/app/shared/services/api/user.service';
import { delay, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {
  form: FormGroup;
  @Input() user: any;
  isLoading: boolean = false;

  autoTips: Record<string, Record<string, string>> = {
    en: {
      required: 'Campo obrigatório',
      email: 'Email inválido'
    }
  };

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private modal: NzModalRef
  ) { 
    
  }

  ngOnInit(): void {
    console.log(this.user);
    this.form = this.fb.group({
      "email": this.fb.control(this.user.email, [Validators.required, Validators.email]),
      "cnpj": this.fb.control(this.user.cnpj, [Validators.required]),
      "phone_number": this.fb.control(this.user.phone_number),
      "state_registration": this.fb.control(this.user.state_registration, [Validators.required]),
      "social_reason": this.fb.control(this.user.social_reason, [Validators.required]),
      "address": this.fb.group({
        "state": this.fb.control(this.user?.address?.state),
        "city": this.fb.control(this.user?.address?.city),
        "street": this.fb.control(this.user?.address?.street),
        "number": this.fb.control(this.user?.address?.number),
        "neighborhood": this.fb.control(this.user?.address?.neighborhood),
        "cep": this.fb.control(this.user?.address?.cep)
      }),
      "type": this.fb.control(this.user.distributor ? "distributor": this.user.station ? "station" : "admin"),
      "bank": this.fb.group({
        "code": this.fb.control(this.user?.distributor?.bank_account?.code),
        "agency": this.fb.control(this.user?.distributor?.bank_account?.agency),
        "account": this.fb.control(this.user?.distributor?.bank_account?.account),
        "type": this.fb.control(this.user?.distributor?.bank_account?.type),
      })
    });
  }

  submitForm(): void {
    this.isLoading = true;
    this.userService.update({ endpoint: 'users', id: this.user.id }, this.form.value)
    .pipe(delay(1000), finalize(() => this.isLoading = false))
    .subscribe((result) => this.destroyModal(result));
  }

  destroyModal(data): void {
    this.modal.destroy(data);
  }

}
