import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnitListComponent } from './unit-list/unit-list.component';
import { UnitRoutingModule } from './unit-routing.module';
import { NzTableModule, NzCardModule, NzInputModule, NzIconModule, NzButtonModule, NzPopconfirmModule, NzSpinModule, NzModalModule, NzFormModule, NzSelectModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UnitService } from 'src/app/shared/services/api/unit.service';
import { NgxMaskModule } from 'ngx-mask';
import { UnitUpdateComponent } from './unit-update/unit-update.component'

const antdModules = [
  NzTableModule,
  NzCardModule,
  NzInputModule,
  NzIconModule,
  NzButtonModule,
  NzPopconfirmModule,
  NzSpinModule,
  NzModalModule,
  NzFormModule,
  NzSelectModule
];

@NgModule({
  declarations: [UnitListComponent, UnitUpdateComponent],
  imports: [
    CommonModule,
    UnitRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ...antdModules,
    NgxMaskModule.forRoot()
  ],
  providers: [UnitService]
})
export class UnitModule { }
