import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { TableService } from 'src/app/shared/services/table.service';
import { UnitService } from 'src/app/shared/services/api/unit.service';
import { finalize, delay } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { UnitUpdateComponent } from '../unit-update/unit-update.component';

@Component({
  selector: 'app-unit-list',
  templateUrl: './unit-list.component.html',
  styleUrls: ['./unit-list.component.css']
})
export class UnitListComponent implements OnInit {

  allChecked: boolean = false;
  indeterminate: boolean = false;
  displayData = [];
  searchInput: string
  autoTips: Record<string, Record<string, string>> = {
    en: {
      required: 'Campo obrigatório',
      email: 'Email inválido'
    }
  };

  orderColumn = [
    {
      title: 'ID',
      compare: (a, b) => a.id.localeCompare(b.id),
    },
    {
      title: 'Nome',
      compare: (a, b) => a.name.localeCompare(b.name)
    },
    {
      title: 'Cpf/Cnpj',
      compare: (a, b) => a.cpf_or_cnpj.localeCompare(b.cpf_or_cnpj)
    },
    {
      title: 'Email',
      compare: (a, b) => a.email.localeCompare(b.email),
    },
    {
      title: 'Estado',
      compare: (a, b) => a.state_abbreviation.localeCompare(b.state_abbreviation)
    },
    {
      title: 'Telefone',
      compare: (a, b) => a.phone.localeCompare(b.phone)
    },
    {
      title: 'Data de criação',
      compare: (a, b) => a.created_at.localeCompare(b.created_at)
    },
    {
      title: 'Última atualização',
      compare: (a, b) => a.updated_at.localeCompare(b.updated_at)
    },
    {
      title: ''
    }
  ]

  fuelList = [];
  isLoading: boolean = false;
  isVisible: boolean = false;
  form: FormGroup;

  constructor(
    private tableSvc: TableService, 
    private unitService: UnitService, 
    private fb: FormBuilder,
    private nzModalService: NzModalService,
    private viewContainerRef: ViewContainerRef
  ) {
    this.form = this.fb.group({
      "state_abbreviation": this.fb.control("", [Validators.required]),
      "email": this.fb.control("", [Validators.required, Validators.email]),
      "cpf_or_cnpj": this.fb.control("", [Validators.required]),
      "name": this.fb.control("", [Validators.required]),
      "phone": this.fb.control("")
    })
  }

  search() {
    this.displayData = this.tableSvc.search(this.searchInput, this.fuelList)
  }

  ngOnInit() { 
    this.loadData();
  }

  toggleModal() {
    this.isVisible = !this.isVisible;
  }

  showModalUpdate(item) {
    const modal = this.nzModalService.create({
      nzTitle: `Atualizar Unidade - ${item.name}`,
      nzContent: UnitUpdateComponent,
      nzViewContainerRef: this.viewContainerRef,
      nzComponentParams: {
        unit: item
      },
      nzOnOk: () => new Promise(resolve => setTimeout(resolve, 1000)),
      nzFooter: [
        {
          label: 'Salvar',
          type: 'primary',
          loading: componentInstance => componentInstance.isLoading,
          onClick: componentInstance => {
            componentInstance.submitForm()
          }
        }
      ]
    })

    modal.afterClose.subscribe(result => {
      if(result) {
        this.loadData() 
      }      
    });

  }

  loadData() {
    this.isLoading = true;
    this.unitService.read({ endpoint: 'units' })
    .pipe(delay(1000), finalize(() => this.isLoading = false))
    .subscribe((data: any) => { 
      this.displayData = data.units
      this.fuelList = data.units
    });
  }

  delete(unit: any) {
    this.unitService.delete({ endpoint: 'units', id: unit.id }).subscribe(() => this.loadData());
  }

  handleOk(): void {
    for (const i in this.form.controls) {
      this.form.controls[i].markAsDirty();
      this.form.controls[i].updateValueAndValidity();
    }
    if(this.form.valid) {
      this.unitService.create({ endpoint: 'units' }, this.form.value).subscribe(() => { 
        this.isVisible = false
        this.loadData()
      })
    }
  }

  handleCancel(): void {
    this.isVisible = false;
  }

}