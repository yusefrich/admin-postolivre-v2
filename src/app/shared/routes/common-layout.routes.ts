import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';

export const CommonLayout_ROUTES: Routes = [
    {
        path: 'dashboard',
        loadChildren: () => import('../../dashboard/dashboard.module').then(m => m.DashboardModule),
    },
    {
        path: 'auctions',
        loadChildren: () => import('../../modules/auction/auction.module').then(m => m.AuctionModule),
    },
    {
        path: 'lotes',
        loadChildren: () => import('../../modules/lot/lot.module').then(m => m.LotModule),
        // canLoad: [AuthGuard],
        canActivate: [AuthGuard]
    },
    {
        path: 'users',
        loadChildren: () => import('../../modules/user/user.module').then(m => m.UserModule),
        // canLoad: [AuthGuard],
        canActivate: [AuthGuard]
    },
    { 
        path: 'fuels',
        loadChildren: () => import('../../modules/fuel/fuel.module').then(m => m.FuelModule),
        canActivate: [AuthGuard]
    },
    { 
        path: 'units',
        loadChildren: () => import('../../modules/unit/unit.module').then(m => m.UnitModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'faq',
        loadChildren: () => import('../../modules/faq/faq.module').then(m => m.FaqModule),
        canActivate: [AuthGuard]
    }
];