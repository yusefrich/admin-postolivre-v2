import { Component, OnInit } from '@angular/core';
import { PusherService } from '../../services/pusher.service';
import { AuthenticationService } from '../../services/authentication.service';
import { NotificationService } from '../../services/api/notification.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {


  notificationList = [];

  constructor(private pusherService: PusherService, private authService: AuthenticationService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.connect();
    this.loadData();
  }

  loadData() {
    this.notificationService.read({ 
      endpoint: 'notifications', 
      search: `notifiable_id:${this.authService.currentUserValue.user.id}`,
      searchFields: `notifiable_id:=`,
      orderBy: 'created_at',
      sortedBy: 'desc'
    }).subscribe((data: any) => this.notificationList = data.notifications);
  }

  connect() {
    this.pusherService.connect();
    let self = this;
    // var channel = this.pusherService.pusher.subscribe('posto-livre');
    // channel.bind('App\\Events\\OpeningOfAuction', function (data) {
    //     console.log(data);
    // });
    let channel2 = this.pusherService.pusher.subscribe(`private-Api.User.Models.User.${this.authService.currentUserValue.user.id}`);
    channel2.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function (data) {
      console.log(data);
      self.notificationList.push(data);
      // switch (data.type) {
      //   case "App\\Notifications\\LotsCreated":
      //     console.log("Os lotes foram criados já pode dar lance!!");
      //     break;
      // }
    });

  }
}
