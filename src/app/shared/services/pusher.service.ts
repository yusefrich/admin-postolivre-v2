import { Injectable } from '@angular/core';
import * as Pusher from 'pusher-js';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

// this is here to discourage the instantianting of pusher any where its
// needed, better to reference it from one place
@Injectable()
export class PusherService {
    public pusher: Pusher.Pusher;
    private isConnected: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    isConnectedChanges: Observable<boolean> = this.isConnected.asObservable();

    constructor(private http: HttpClient) {}

    connect(): void {
        this.pusher = new Pusher('ABCDEFG', {
            wsHost: environment.pusherHost,
            wsPort: 6001,
            // httpHost: environment.baseUrl,
            disableStats: true,
            authEndpoint: `${environment.baseUrl}/laravel-websockets/auth`,
            auth: {
                headers: {
                    'X-App-ID': 123456
                }
            },
            enabledTransports: ['ws']
        });

        this.pusher.connection.bind('connected', () => {
            this.isConnected.next(true);
        });
    }

    disconnect() {
        this.pusher.disconnect();
        this.isConnected.next(false);
    }

}
