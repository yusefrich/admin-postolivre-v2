import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd';
import { AuthenticationService } from '../authentication.service';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  protected baseUrl: string = environment.baseUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'my-auth-token'
    }),
    params: new HttpParams()
  };


  constructor(protected http: HttpClient, protected notification: NzNotificationService, protected authService: AuthenticationService) { }

  create<T>(model: T | any, data: T | any): Observable<T  | T[]> {
    return this.http.post<T | T[]>(this.baseUrl + '/api/v1/' + model.endpoint, data, this.buildHttpOptions(model))
      .pipe(
        catchError(this.handleError.bind(this))
      )
    ;
  }
  upload<T>(model: T | any, data: T | any): Observable<T  | T[]> {

    return this.http.post<T | T[]>(this.baseUrl + '/api/v1/' + model.endpoint, data, this.fileHttpOptions(model))
      .pipe(
        catchError(this.handleError.bind(this))
      )
    ;
  }

  read<T>(model: T| any): Observable<T | T[]> {
    return this.http.get<T | T[]>(this.baseUrl + '/api/v1/' + model.endpoint, this.buildHttpOptions(model))
      .pipe(
        catchError(this.handleError.bind(this))
      )
    ;
  }

  update<T>(model: T | any, data: T | any): Observable<T | T[]> {
    return this.http.put<T | T[]>(this.baseUrl + `/api/v1/${model.endpoint}/${model.id}`, data, this.buildHttpOptions(model))
    .pipe(
      catchError(this.handleError.bind(this))
    )
  }

  delete<T>(model: T| any): Observable<T | T[]> {
    return this.http.delete<T | T[]>(this.baseUrl + `/api/v1/${model.endpoint}/${model.id}`, this.buildHttpOptions(model));
  }

  restore<T>(model: T | any): Observable<T | T[]> {
    return this.http.put<T | T[]>(this.baseUrl + `/api/v1/${model.endpoint}/${model.id}/restore`, {}, this.buildHttpOptions(model));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
    } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        this.notification.create("error", "Erro", error.error.error.message)

        console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
        'Something bad happened; please try again later.');
  };

  private buildHttpOptions(model: any) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+ this.authService.currentUserValue.access_token
    });
    let params = new HttpParams();
    if(model.includes) {
      params = params.append('include', model.includes);
    }

    if(model.search) {
      params = params.append('search', model.search);
    }

    if(model.searchFields) {
      params = params.append('searchFields', model.searchFields);
    }

    if(model.searchJoin) {
      params = params.append('searchJoin', model.searchJoin);
    }

    if(model.orderBy) {
      params = params.append('orderBy', model.orderBy);
    }

    if(model.sortedBy) {
      params = params.append('sortedBy', model.sortedBy);
    }

    return {
      headers: headers,
      params: params
    }
  }

  private fileHttpOptions(model: any) {
    console.log("fileHttpOptions being successully called");
    let headers = new HttpHeaders({
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+ this.authService.currentUserValue.access_token
    });
    let params = new HttpParams();
    if(model.includes) {
      params = params.append('include', model.includes);
    }

    if(model.search) {
      params = params.append('search', model.search);
    }

    if(model.searchFields) {
      params = params.append('searchFields', model.searchFields);
    }

    if(model.searchJoin) {
      params = params.append('searchJoin', model.searchJoin);
    }

    if(model.orderBy) {
      params = params.append('orderBy', model.orderBy);
    }

    if(model.sortedBy) {
      params = params.append('sortedBy', model.sortedBy);
    }

    return {
      headers: headers,
      params: params
    }
  }
}
