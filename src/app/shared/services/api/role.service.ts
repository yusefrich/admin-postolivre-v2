import { Injectable } from '@angular/core';
import { BaseService } from './base-service.service';
import { HttpClient } from '@angular/common/http';
import { NzNotificationService } from 'ng-zorro-antd';
import { AuthenticationService } from '../authentication.service';

@Injectable({
  providedIn: 'root'
})
export class RoleService extends BaseService {

  constructor(http: HttpClient, notification: NzNotificationService, authService: AuthenticationService) { 
    super(http, notification, authService);
  }
}
