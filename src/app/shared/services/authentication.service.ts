import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { User, LoginResponse } from '../interfaces/user.type';
import { NzNotificationService } from 'ng-zorro-antd';
import { Router } from '@angular/router';

const USER_AUTH_API_URL = `${environment.baseUrl}/api/v1/login`;

@Injectable()
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<LoginResponse>;
    public currentUser: Observable<LoginResponse>;

    constructor(private http: HttpClient, private notification: NzNotificationService, private router: Router) {
        this.currentUserSubject = new BehaviorSubject<LoginResponse>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): LoginResponse {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(USER_AUTH_API_URL, { username, password })
            .pipe(
                map(user => {
                    console.log(user);
                    if (user) {
                        localStorage.setItem('currentUser', JSON.stringify(user));
                        this.currentUserSubject.next(user);
                    }
                    return user;
                }),
                catchError(this.handleError.bind(this))
            );
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        this.router.navigate(['/auth/login']);
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            this.notification.create("error", "Erro", error.error.error.message)

            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };
}