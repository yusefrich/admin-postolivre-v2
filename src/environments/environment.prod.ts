export const environment = {
  production: true,
  baseUrl: 'https://api.postolivre.com',
  pusherHost: 'websocket.postolivre.com'
};
